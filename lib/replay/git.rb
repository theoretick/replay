require 'logger'
require 'tmpdir'

module Replay
  # Git module handles repository interactions
  class Git

    DEFAULT_DEPTH = 10

    attr_reader :repo_path, :prev_ref

    def self.rm_clone(path)
      FileUtils.remove_entry(path)
    end

    def initialize(repo_path, depth, commit_since, commit_until)
      @repo_path = repo_path
      @commit_depth = depth
      @commit_since = commit_since
      @commit_until = commit_until
      @prev_ref = git("rev-parse --abbrev-ref HEAD").chop
    end

    def clone!
      temp_path = Dir.mktmpdir("replay-#{Time.now}-", "test")

      FileUtils.cp_r(@repo_path+"/.", temp_path)

      # Remove index lock if it was copied over
      index_lock = temp_path+"/.git/index.lock"
      FileUtils.remove_entry(index_lock) if File.exist?(index_lock)


      self.class.new(
        temp_path,
        @commit_depth,
        @commit_since,
        @commit_until
      )
    end

    def timestamp(sha)
      git("show --no-patch --no-notes --pretty='%ct' #{sha}").chop
    end

    def checkout(sha)
      git("checkout -q #{sha}")
    end

    # partition commit_range into max thread count
    def range_slice(thread_count)
      threads = thread_count.to_i.times.map { [] }
      i = 0
      commit_range.each do |commit|
        i = 0 if i >= thread_count

        threads[i] << commit
        i += 1
      end

      threads
    end

    def commit_range
      if @commit_since && @commit_until
        git("rev-list HEAD --since=#{@commit_since} --until=#{@commit_until}").split("\n")
      elsif @commit_depth
        git("rev-list HEAD --max-count=#{@commit_depth}").split("\n")
      else
        raise "cannot generate commit range"
      end
    end

    def git(cmd)
      `git -C #{repo_path} #{cmd}`
    end
  end
end