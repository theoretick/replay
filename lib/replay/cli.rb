# frozen_string_literal: true

require 'logger'
require 'optparse'

require 'replay/analyzer'
require 'replay/git'
require 'replay/metrics'

module Replay
  LOGGER = Logger.new($stdout)

  module CLI
    BANNER = <<~BANNER

    Replay

  Usage: replay [options] REPO_PATH
BANNER

    CONFIG = {
      depth: ENV.fetch("DEPTH", nil),
      since: nil,
      until: nil,
      image: ENV.fetch("IMAGE", Replay::Analyzer::DEFAULT_IMAGE),
      project: ENV.fetch('PROJECT', nil),
      logger: LOGGER
    }

    def self.run
      LOGGER.warn!

      begin
        parse_args
      rescue => e
        LOGGER.error(e.to_s)
        exit 1
      end
      conf = CONFIG


      validate(conf)

      report_path = Analyzer.new(conf).execute

      Metrics.new(report_path).execute
    end

    def self.parse_args
      OptionParser.new do |opts|
        opts.banner = BANNER
        opts.on('--depth=DEPTH', "commit depth (DEPTH=#{Replay::Git::DEFAULT_DEPTH})")
        opts.on('--since=SINCE', "commits more recent than a specific date")
        opts.on('--until=UNTIL', "commits until a specific date")
        opts.on('--image=IMAGE', "analyzer container image (IMAGE=#{CONFIG[:image]})")
        opts.on('--project=PROJECT', 'repository path (PROJECT)')
        opts.on('--threads=THREADS', "max thread count (THREADS=#{Replay::Analyzer::DEFAULT_THREAD_COUNT})")
        opts.on('-d', '--debug', 'Run in debug mode') do |_d|
          LOGGER.debug!
        end
        opts.on('-h', '--help', 'Print help message') do
          LOGGER.info(opts)
          exit 1
        end
      end.parse!(into: CONFIG)
    end

    def self.validate(conf)
      if conf[:depth] && (conf[:since] || conf[:until])
        LOGGER.error("*** cannot specify both depth and since/until range")
        exit 1
      end

      if (conf[:since] && conf[:until].nil?) || (conf[:until] && conf[:since].nil?)
        LOGGER.error("*** must specify both since and until range")
        exit 1
      end

      if conf[:project].nil?
        LOGGER.error('*** no project path provided, exiting...')
        exit 1
      end
    end
  end
end
