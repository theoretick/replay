# frozen_string_literal: true

require 'csv'
require 'json'
require 'pathname'
require 'tmpdir'

module Replay
  # Metrics takes a directory of reports and builds a metrics collection
  #
  # The delta returned corresponds to an overall reduction in counts between
  # locations_with_tracking and the specific signature algorithms.
  #
  # Note that we use `locations_with_tracking` over `locations` as there are cases
  # where a signature is not generated. In those instances, it would be inaccurate
  # to measure efficacy as there will always be a line number but not always a scope.
  class Metrics
    TABLE_WIDTH = 84
    FINGERPRINT_COL_WIDTH = 40
    COUNT_COL_WIDTH = 6
    DELTA_COL_WIDTH = 34

    CSV_HEADERS = [
      "distinct_fingerprints",
      "total_count",
      "uniq_count",
      "delta_v_locations_with_tracking"
    ].freeze

    def initialize(reports_path)
      @reports_path = reports_path
    end

    def execute(histogram=true)
      if histogram
        headers, rows = build_histogram_rows
      else
        headers, rows = build_cumulative_rows
      end

      render_csv(headers, rows)
    end

    private

    def build_histogram_rows
      headers = ['timestamp', 'sha', *CSV_HEADERS]
      rows = []

      # Sort by oldest first to build cumulatively
      # since we analyzer in reverse chronological order
      Dir[@reports_path+"/**"].
        each_with_object([]) do |file, paths|
          # Build each metric collection cumulatively on past entries
          paths << file
          metrics = parse_metrics(paths)

          basename = Pathname.new(file).basename
          timestamp, sha, _ = basename.to_s.split('.')
          rows_for_file = build_csv_rows(metrics)
          rows.concat(
            rows_for_file.map{ |row| [timestamp, sha, *row]}
          )
        end

      [headers, rows]
    end

    def build_cumulative_rows
      metrics = parse_metrics(
        Dir[@reports_path+"/**"]
      )

      [
        CSV_HEADERS,
        build_csv_rows(metrics)
      ]
    end

    def render_csv(headers, rows)
      puts headers.join(',')
      rows.each do |row|
        puts row.join(',')
      end
    end

    def build_csv_rows(metrics)
      rows = []

      # These two are special and should be extracted first
      locations = metrics.delete("locations")
      locations_with_tracking = metrics.delete("locations_with_tracking")

      raise "ERROR: Missing locations_with_tracking" if locations_with_tracking.length < 1

      delta = ->(set, baseline) { (1-(set.length/baseline.length.to_f)).round(2) }
      build_row = ->(type, items, delta) { [type, items.length.to_s, items.uniq.length.to_s, delta.to_s] }

      rows << build_row.call('locations', locations, delta.call(locations.uniq, locations_with_tracking.uniq))
      rows << build_row.call('locations_with_tracking', locations_with_tracking, delta.call(locations_with_tracking.uniq, locations_with_tracking.uniq))

      metrics.each do |metric, set|
        rows << build_row.call(
          metric,
          set,
          delta.call(set.uniq, locations_with_tracking.uniq)
        )
      end

      rows
    end

    def parse_metrics(report_paths)
      metrics = {
        'locations' => Array.new,
        'locations_with_tracking' => Array.new
      }

      report_paths.each do |report_path|
        report = JSON.parse(File.read(report_path))

        line_numbers = report['vulnerabilities'].map { |v| v.dig('location','start_line') }
        metrics['locations'].concat(line_numbers)

        vulns_with_tracking = report['vulnerabilities'].filter { |v| v.dig('tracking', 'items') }

        # We can only measure the efficacy of vulnerabilities that have both a location and tracking so
        # we have comparative baselines between trackable vulnerabilities
        next unless vulns_with_tracking.any?

        line_numbers = vulns_with_tracking.map { |v| v.dig('location','start_line') }
        metrics['locations_with_tracking'].concat(line_numbers)

        tracking_items = vulns_with_tracking.map { |v| v.dig('tracking', 'items') }.compact
        next unless tracking_items.any?

        tracking_items.
          flatten.
          map { |item| item.dig('signatures') }.
          flatten.
          group_by { |sig| sig.dig('algorithm') }.
          map do |algo, sigs|
            values = sigs.map { |sig| sig.dig('value') }

            metrics[algo] ||= Array.new
            metrics[algo].concat(values)
          end
      end

      metrics
    end
  end
end