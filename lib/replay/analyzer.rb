require 'logger'
require 'tmpdir'

require_relative "./git"

module Replay
  # Analyzer takes a repository path and (optional) depth to generate reports per commit
  class Analyzer
    DEFAULT_IMAGE_TAG = ENV["ANALYZER_IMAGE_TAG"] || "4"
    DEFAULT_IMAGE = "registry.gitlab.com/security-products/semgrep:#{DEFAULT_IMAGE_TAG}"
    DEFAULT_THREAD_COUNT = 1
    THREAD_TIMEOUT_SECONDS = 600

    DEFAULT_ENV = {
      'ANALYZER_INDENT_REPORT' => 'false',
      'CI_PROJECT_DIR' => '/tmp/app',
      'GITLAB_FEATURES' => 'vulnerability_finding_signatures',
      'SECURE_LOG_LEVEL' => 'warn',
      # Deduplicate signatures since we rely entirely off total counts
      'SAST_TRACKING_CALCULATOR_DEDUPLICATE' => 'false'
    }

    attr_reader :repo

    def initialize(config)
      @thread_count = (config[:threads] || DEFAULT_THREAD_COUNT).to_i
      @image = config.fetch(:image)
      @project_path = config.fetch(:project)

      @repo = Replay::Git.new(@project_path, config[:depth], config[:since], config[:until])
    end

    def execute
      report_path = Dir.mktmpdir("reports-#{Time.now}-", "test")

      LOGGER.debug("Running analysis against #{@project_path}...")
      LOGGER.debug("Saving reports to #{report_path} ...")

      scan_concurrent(repo.commit_range, report_path)

      report_path
    end

    def scan_sequential(commit_range, report_path)
      # Iterate over each commit, running analyzer per commit and copying report to reportsPath
      commit_range.each do |commit|
        scan_commit(commit, report_path)
      end
    end

    def scan_concurrent(commit_range, report_path)
      LOGGER.debug("scanning #{commit_range.length} commits using #{@thread_count} threads...")

      threads = []
      repo.range_slice(@thread_count).each do |slice|
        next unless slice.any?

        threads << Thread.new do
          slice.each do |commit|
            scan_commit(commit, report_path)
          end
        end.run
      end
      threads.each { |t| t.join(THREAD_TIMEOUT_SECONDS) }
    end

    def scan_commit(sha, report_path)
      temp_repo = repo.clone!

      commit_timestamp = repo.timestamp(sha)
      temp_repo.checkout(sha)

      docker_run(temp_repo.repo_path, @image)

      FileUtils.cp(
        "#{temp_repo.repo_path}/gl-sast-report.json",
        "#{report_path}/#{commit_timestamp}.#{sha}.json"
      )
      Git.rm_clone(temp_repo.repo_path)
    end

    def docker_run(repo_path, image_name)
      opts = ["--rm", "--platform linux/amd64"]
      opts << DEFAULT_ENV.map { |k, v| "-e #{k}=#{v}" }
      opts << ["-v", "#{Dir.pwd + "/" + repo_path}:/tmp/app", "-w", "/tmp/app"]

      `docker run #{opts.join(" ")} #{image_name}`
    end
  end
end