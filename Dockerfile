FROM docker:20.10

RUN apk add --no-cache ruby ruby-bundler git ruby-dev make gcc musl-dev R

COPY . /build
WORKDIR /build

RUN R -e 'install.packages("tidyverse", repos="https://ftp.osuosl.org/pub/cran")'

