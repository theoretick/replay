# Patched semgrep that includes `scope_offset_compressed` signatures including split algorithms
IMAGE ?= registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep:bump-tc
DEPTH ?= 30
THREADS ?= 10

test-clean:
	rm -rf test/replay-*
	rm -rf test/reports-*

e2e-ruleset:
	[ -d test/ruleset ] || git clone https://gitlab.com/gitlab-org/security-products/analyzers/ruleset.git test/ruleset
	IMAGE=$(IMAGE) ./exe/replay --project=test/ruleset --depth=$(DEPTH) --thread=$(THREADS) > test/ruleset-data.csv

e2e-gitaly:
	[ -d test/gitaly ] || git clone https://gitlab.com/gitlab-org/gitaly.git test/gitaly
	IMAGE=$(IMAGE) ./exe/replay --project=test/gitaly --depth=$(DEPTH) --thread=$(THREADS) > test/gitaly-data.csv

e2e-scope-tracking-benchmark:
	[ -d test/scope-tracking-benchmark ] || git clone https://gitlab.com/theoretick/scope-tracking-benchmark.git test/scope-tracking-benchmark
	IMAGE=$(IMAGE) ./exe/replay --project=test/scope-tracking-benchmark --depth=$(DEPTH) --thread=$(THREADS) > test/scope-tracking-benchmark-data.csv


###################################################################################################

plot-ruleset: test/ruleset-data.csv
	Rscript plot_counts.r test/ruleset-data.csv

plot-gitaly: test/gitaly-data.csv
	Rscript plot_counts.r test/gitaly-data.csv

plot-scope-tracking-benchmark: test/scope-tracking-benchmark-data.csv
	Rscript plot_counts.r test/scope-tracking-benchmark-data.csv
