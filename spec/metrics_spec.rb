require "spec_helper"
require_relative '../lib/replay/metrics'

require 'csv'

RSpec.describe Replay::Metrics do
  describe '#execute' do
    it 'renders valid csv' do
      # Set up standard output as a StringIO object.
      capture = StringIO.new
      $stdout = capture

      Replay::Metrics.new("./spec/fixtures/reports").execute


      expect(
        CSV.parse(capture.string)
      ).to eql(
        [
          ["timestamp", "sha", "distinct_fingerprints", "total_count", "uniq_count", "delta_v_locations_with_tracking"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "locations", "1", "1", "0.0"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "locations_with_tracking", "1", "1", "0.0"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "scope_offset_compressed", "1", "1", "0.0"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "scope_offset_compressed_only", "1", "1", "0.0"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "scope_offset_type_identifier", "1", "1", "0.0"],
          ["1686588068", "cab5b30443b6c022475283dcb786d5831762c37a", "scope_offset", "1", "1", "0.0"]
        ]
      )
    end
  end
end