require "spec_helper"
require_relative '../lib/replay/git'

require 'csv'

RSpec.describe Replay::Git do
  describe '#rm_clone' do
    it 'removes path' do
      tmpdir = Dir.mktmpdir("", "/tmp")

      expect{
        Replay::Git.rm_clone(tmpdir)
      }.to change {
        Dir[tmpdir]
      }
    end
  end

  describe "#timestamp" do
    it 'returns the unixtimestamp for a given SHA' do
      svc = Replay::Git.new("spec/fixtures/scope-tracking-benchmark", 1, nil, nil)

      expect(
        svc.timestamp(svc.prev_ref)
      ).to eq "1686588068"
    end
  end

  describe "#commit_range" do
    it 'accepts depths' do
      depth = 1
      svc = Replay::Git.new("spec/fixtures/scope-tracking-benchmark", depth, nil, nil)
      expect(svc.commit_range.length).to eq(1)

      depth = 3
      svc = Replay::Git.new("spec/fixtures/scope-tracking-benchmark", depth, nil, nil)
      expect(svc.commit_range.length).to eq(3)
    end

    it 'accepts ranges' do
      svc = Replay::Git.new(
        "spec/fixtures/scope-tracking-benchmark",
        nil,
        '2023-06-10',
        '2023-06-13'
      )
      expect(svc.commit_range.length).to eq(6)
    end
  end

  describe '#range_slice' do
    it 'partitions commits into thread counts' do
      depth = 6
      svc = Replay::Git.new("spec/fixtures/scope-tracking-benchmark", depth, nil, nil)
      slices = svc.range_slice(6)
      expect(slices.length).to eq(6)
      slices.each do |slice|
        expect(slice.length).to eq(1)
      end

      slices = svc.range_slice(3)
      expect(slices.length).to eq(3)
      slices.each do |slice|
        expect(slice.length).to eq(2)
      end
    end
  end
end