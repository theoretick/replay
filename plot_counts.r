#!/usr/bin/env Rscript

library(tidyverse)
require(ggplot2)

args <- commandArgs(trailingOnly = TRUE)

if (length(args) == 0) {
    filename <- "test/data.csv"
} else if (length(args) == 1) {
    filename <- args[1]
}

csv <- read.csv(filename, header = TRUE) %>%
  group_by(distinct_fingerprints)

graph_plain <- csv %>%
  ggplot() +
    aes(x = as.POSIXct(timestamp, origin="1970-01-01", group = distinct_fingerprints)) +
    # Apply jitter to better distinguish overlapping lines
    geom_line(aes(y = uniq_count, colour = distinct_fingerprints), position=position_jitter(w=0.1, h=0.1)) +
    ggtitle(filename) +
    ylab("# Unique trackings") +
    xlab("timestamp") +
    theme(text = element_text(size = 16, family = "Helvetica"))

graph_facet <- graph_plain +
  facet_wrap(~distinct_fingerprints)

filename_out <- unlist(strsplit(filename, split = ".", fixed = TRUE)[1])[1]

ggsave(paste(filename_out, ".plain.png", sep = ""), plot = graph_plain)
ggsave(paste(filename_out, ".facet.png", sep = ""), plot = graph_facet)
