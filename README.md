# Replay

Replay attempts to measure the efficacy of vulnerability tracking signatures
and because I couldn't quickly get
[sourcewarp](https://gitlab.com/gitlab-org/vulnerability-research/foss/sourcewarp/-/blob/main/README.md)
working, I'm basically re-implementing it...

## Installation

1. `R`
    1. `install.packages('tidyverse')`


## Evaluation

This benchmark will execute scans for every commit in a given range and accumulate tracking details.

The metrics returned are cumulative unique occurrences of a given tracking method; i.e. locations or signatures.

For example, to measure the efficacy of `scope_offset` we execute a scan against the trailing 10 commits (depth) of a given
repository. The results may look as follows:

```sh
❯ ./exe/replay --project=/tmp/replay-test --depth=10 | perl -pe 's/((?<=,)|(?<=^)),/ ,/g;' | column -t -s,

timestamp   distinct_fingerprints    total_count  uniq_count  delta_v_locations_with_tracking
...
1686583132  locations                100          30          -0.15
1686583132  locations_with_tracking  80           26          0.0
1686583132  scope_offset_compressed  80           8           0.69
1686583132  scope_offset             70           7           0.73
```

For the 10 commits the benchmark ran against:
- 10 `gl-sast-report.json` were generated (one scan per commit)
- 100 total vulnerability locations were present in the reports
- 30 locations were unique
- 26 unique locations had an accompanying `tracking` property (vuln was considered "trackable" by any known signature algorithm)
- 8 unique `scope_offset_compressed` signatures were generated
- 7 unique `scope_offset` signatures were generated
- The `scope_offset_compressed` algorithm successfully generated signatures in +80% of cases
- The `scope_offset_compressed` algorithm reduced total unique trackings by +69% (8/26)
- The `scope_offset_compressed` algorithm generated +12% more trackings than `scope_offset`

When plotted over a time series, we can track comparative growth

### Execution Steps

1. Specify local repository
1. Checkout each commit in a provided range:
    1. Run analyzer container
    1. Record cumulative:
        1. Distinct line numbers
        1. Distinct signatures of each type
1. Calculate efficacy of signatures by generating delta of locations and signature types

## Usage

### Docker

```
rm -rf test
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v $PWD:$PWD -w $PWD replay:latest make e2e-scope-tracking-benchmark && make plot-scope-tracking-benchmark
open test/scope-tracking-benchmark-data.facet.pdf
```

### Customized usage

```sh
❯ git clone -q git@gitlab.com:gitlab-org/security-products/analyzers/ruleset.git /tmp/ruleset-replay-test

❯ time ./exe/replay --project=/tmp/ruleset-replay-test --depth=1
Running analysis against /tmp/ruleset-replay-test...
Saving reports to /tmp/reports-2023-06-08120648-0700-20230608-39710-lksiwz...

                   Distinct fingerprints | Count |  Rel Δ to locations_with_tracking
------------------------------------------------------------------------------------
                              locations  |    10 |
                 locations_with_tracking |     7 |                               0.0
                            scope_offset |     7 |                               0.0
./exe/replay --project=/tmp/ruleset-replay-test --depth=1  0.06s user 0.07s system 1% cpu 7.277 total

❯ time ./exe/replay --project=/tmp/ruleset-replay-test --depth=5
Running analysis against /tmp/ruleset-replay-test...
Saving reports to /tmp/reports-2023-06-08120700-0700-20230608-39844-p4c8q0...

                   Distinct fingerprints | Count |  Rel Δ to locations_with_tracking
------------------------------------------------------------------------------------
                              locations  |    20 |
                 locations_with_tracking |    15 |                               0.0
                            scope_offset |     7 |                              0.53
./exe/replay --project=/tmp/ruleset-replay-test --depth=5  0.15s user 0.21s system 1% cpu 35.506 total
```

## Tests

- Unit: `bundle exec rspec`
- Integration: `make e2e-scope-tracking-benchmark`